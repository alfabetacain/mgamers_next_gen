package controllers

import javax.inject._

import dao.UserDAO
import models.{User, UserFormSignup}
import play.api._
import play.api.data._
import play.api.data.Forms._
import play.api.data.format.Formats._
import play.api.db.slick.DatabaseConfigProvider
import play.api.mvc._
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import slick.driver.JdbcProfile

import scala.concurrent.Future
import models.Navbar

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class HomeController @Inject()(val messagesApi: MessagesApi, dbConfigProvider: DatabaseConfigProvider,
                               val userDAO: UserDAO) extends Controller with I18nSupport {

  val userForm = Form(
    mapping(
      "userID" -> nonEmptyText,
      "password" -> nonEmptyText
    )(UserFormSignup.apply)(UserFormSignup.unapply)
  )
  /**
   * Create an Action to render an HTML page.
   *
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */
  def index = Action { implicit request =>
      //Ok(views.html.signup(userForm))
      Ok(views.html.index(Some(Navbar.home)))
  }
  def main = Action { implicit request =>
    Ok(views.html.main("Main page"){null})
  }
  def about = Action { implicit request =>
    Ok(views.html.index(Some(Navbar.about)))
  }
  def contact = Action { implicit request =>
    Ok(views.html.index(Some(Navbar.contact)))
  }

  def signup = Action.async { implicit request =>
    userForm.bindFromRequest().fold(
      blah => Future.successful(Ok(views.html.index(Some("Home")))),
      userData =>
        userDAO.find(userData.userID).map{ opt =>
          opt.map(_ => Ok(views.html.index(Some("Home")))).getOrElse(Ok(views.html.index(Some("Home"))))
        }
    )
  }
}
