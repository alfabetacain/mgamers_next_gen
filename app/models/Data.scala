package models

object Navbar {
  val home = "Home"
  val about = "About"
  val contact = "Contact"
}
