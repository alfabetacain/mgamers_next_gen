package models

case class User(userID: String, firstName: String, lastName: String, email: String, password: String)

case class UserFormSignup(userID: String, password: String)