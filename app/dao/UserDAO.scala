package dao

import javax.inject.Inject

import models.User
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.driver.JdbcProfile
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import scala.concurrent.Future

class UserDAO @Inject() (protected val dbConfigProvider: DatabaseConfigProvider) extends HasDatabaseConfigProvider[JdbcProfile] {
  import dbConfig.driver.api._

  private val Users = TableQuery[UserTable]

  def all(): Future[Seq[User]] = db.run(Users.result)

  def find(userID: String): Future[Option[User]] =
    db.run(Users.filter(_.userID === userID).result.headOption)

  def insert(user: User): Future[Unit] = db.run(Users += user).map{ _ => () }

  private class UserTable(tag: Tag) extends Table[User](tag, "User") {
    def userID = column[String]("userID", O.PrimaryKey)
    def firstName = column[String]("firstName")
    def lastName = column[String]("lastName")
    def email = column[String]("email")
    def password = column[String]("password")

    def * = (userID, firstName, lastName, email, password) <> (User.tupled, User.unapply _)
  }
}
