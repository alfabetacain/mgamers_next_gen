# Mgamers next gen server

This is a toy project about reimplementing the mgamers website in Scala instead of php.

Scala has static types and does not do as much type coercion as php does. This
should result in more maintainable code.

The project uses the Play framework for Scala, version 2.5.x (something). 

Authentication is not implemented as of this moment.

## Features to come

- Implement authentication
- Implement event signup (maybe using websockets?)
- Implement HTTPS

## Tech stack so far

- Scala
- Play framework
- Bootstrap

### Future additions

- Scala.js
