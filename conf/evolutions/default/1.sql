# Users schema

# --- !Ups

CREATE TABLE "User" (
    "userID" VARCHAR NOT NULL PRIMARY KEY,
    "firstName" VARCHAR NOT NULL,
    "lastName" VARCHAR NOT NULL,
    "email" VARCHAR NOT NULL,
    "password" VARCHAR NOT NULL
)

# --- !Downs
DROP TABLE "User";
