name := """play-scala-seed"""
organization := "dk.mgamers"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.8"

libraryDependencies += filters
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "1.5.1" % Test
libraryDependencies += "org.webjars" %% "webjars-play" % "2.5.0"
libraryDependencies += "org.webjars" % "bootstrap" % "3.3.5"


libraryDependencies += "com.typesafe.play" %% "play-slick" % "2.0.2"
libraryDependencies += "com.typesafe.slick" %% "slick" % "3.1.1"
//libraryDependencies += "com.typesafe.play" %% "play-jdbc-api" % "2.5.13"
//libraryDependencies += "com.typesafe.play" %% "play-jdbc-evolutions" % "2.5.13"


libraryDependencies += "com.typesafe.slick" %% "slick-hikaricp" % "3.1.1"
libraryDependencies += "com.typesafe.play" %% "play-slick-evolutions" % "2.0.2"
libraryDependencies += "com.h2database" % "h2" % "1.4.194"
//

// Adds additional packages into Twirl
//TwirlKeys.templateImports += "dk.mgamers.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "dk.mgamers.binders._"
